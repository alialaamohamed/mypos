<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{


    
    public function __construct()
    {
            $this->middleware(['permission:categories_read'])->only('index');
            $this->middleware(['permission:categories_create'])->only('create');
            $this->middleware(['permission:categories_update'])->only('edit');
            $this->middleware(['permission:categories_delete'])->only('destroy');
    }

    public function index(Request $request)
    {
        $categories = category::when($request->search,function($q) use ($request){

            return $q->where('name_ar','like','%' .$request->search .'%')
            ->orwhere('name_en','like','%' .$request->search .'%');

        })->latest()->paginate(5);
        
        return view('dashboard.categories.index',compact('categories'));

    }

    
    public function create()
    {
      return view('dashboard.categories.create');
    }

  
    public function store(Request $request)
    {
        $request->validate([

            'name_ar' =>'required|unique:categories,name_ar',
            'name_en' =>'required|unique:categories,name_en',

        ]);


        Category::create($request->all());

        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.categories.index');
    }

 


    public function edit(category $category)
    {
        return view('dashboard.categories.edit', compact('category'));
    }

   
    public function update(Request $request, category $category)
    {
        
        $request->validate([

            'name_ar'    =>'required|unique:categories,name_ar,' . '$category->id',
            'name_en'    =>'required|unique:categories,name_en,' . '$category->id',

        ]);

        
        $category->update($request->all());

        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.categories.index');
    }

    
    public function destroy(category $category)
    {
        $category->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.categories.index');



    }
}
