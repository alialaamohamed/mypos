<?php

namespace App\Http\Controllers\Dashboard;

use App\Category;
use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;



class ProductController extends Controller
{

    public function __construct()
    {
            $this->middleware(['permission:products_read'])->only('index');
            $this->middleware(['permission:products_create'])->only('create');
            $this->middleware(['permission:products_update'])->only('edit');
            $this->middleware(['permission:products_delete'])->only('destroy');
    }



    
    public function index(Request $request)
    {

        $categories =Category::all();

        $products = Product::when($request->search, function ($q) use ($request) {

            return $q->where('titel_ar', 'like', '%' . $request->search . '%')

                    ->orwhere('titel_en','like','%' .$request->search .'%');

        })->when($request->category_id, function ($q) use ($request) {

            return $q->where('category_id', $request->category_id);

        })->latest()->paginate(5);

        return view('dashboard.products.index',compact('categories','products'));
    }

   
    public function create()
    {
        $categories = Category::all();
        return view('dashboard.products.create', compact('categories'));



    }
    public function store(Request $request)
    {
        //dd($request->all());
        $request->validate([
            
            'category_id'        =>'required',
            'titel_ar'            =>'required',
            'descraption_ar'     =>'required',
            'titel_en'           =>'required',
            'descraption_en'     =>'required',
            'image'              =>'image',
            'purchase_price'     =>'required',
            'sale_price'         =>'required',
            'stock'              =>'required'

        ]);


        $request_data =$request ->all();

        if ($request->image) {
            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/product_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of if


        Product::create($request_data);
        session()->flash('success', __('site.added_successfully'));
        return redirect()->route('dashboard.products.index');


    }

    
  

    public function edit(Product $product)
    {
        $categories = Category::all();
        return view('dashboard.products.edit', compact('categories','product'));
    }

  
    public function update(Request $request, Product $product)
    {
        $request->validate([
            
            'category_id'        =>'required',
            'titel_ar'            =>'required',
            'descraption_ar'     =>'required',
            'titel_en'           =>'required',
            'descraption_en'     =>'required',
            'image'              =>'image',
            'purchase_price'     =>'required',
            'sale_price'         =>'required',
            'stock'              =>'required'

        ]);



        
        $request_data = $request->all();

        if ($request->image) {

            if ($product->image != 'default.jepg') {

                Storage::disk('public_uploads')->delete('/product_images/' . $product->image);

            }//end of inner if

            Image::make($request->image)
                ->resize(300, null, function ($constraint) {
                    $constraint->aspectRatio();
                })
                ->save(public_path('uploads/product_images/' . $request->image->hashName()));

            $request_data['image'] = $request->image->hashName();

        }//end of external if

    

        $product->update($request_data);
       
    
        session()->flash('success', __('site.updated_successfully'));
        return redirect()->route('dashboard.products.index');
    }//end of update


    
    public function destroy(Product $product)
    {
        if ($product->image != 'default.jpeg') {

            Storage::disk('public_uploads')->delete('/product_images/' . $product->image);

        }//end of if


        $product->delete();
        session()->flash('success', __('site.deleted_successfully'));
        return redirect()->route('dashboard.products.index');
    }
}
