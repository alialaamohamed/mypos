<?php

Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath' ]
    ], function(){ 

        Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function(){

            Route::get('/','WelcomeController@index')->name('welcome');


            //Users route
            Route::resource('users','UserController')->except(['show']);

            //Categoris route
            Route::resource('categories','CategoryController')->except(['show']);

             //products route
             Route::resource('products','productController')->except(['show']);
            

            //clients route
             Route::resource('clients','clientController')->except(['show']);
             Route::resource('clients.orders','Clients\OrderController')->except(['show']);

            //order routes
            Route::resource('orders', 'OrderController');
            Route::get('/orders/{order}/products', 'OrderController@products')->name('orders.products');
            
            
            
            
            }); //end of dashboard route
    
    });





