
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    @if (app() -> getlocale() == 'ar')
    <ul style='margin-right: 80%;' class="navbar-nav ml-auto">


    {{--<!-- Tasks: style can be found in dropdown.less -->--}}
                    <li class="nav-item dropdown">
                        <a href="#" class="nav-link" data-toggle="dropdown"><i class="fa fa-flag"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                {{--<!-- inner menu: contains the actual data -->--}}
                                <ul class="nav">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li>
                                            <a class="dropdown-item"rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                {{ $properties['native'] }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>

      {{--<!-- User Account: style can be found in dropdown.less -->--}}
                    <li class="nav-item dropdown"  style="margin-top: -2%;">
                        <a href="#" class="nav-link user-panel" data-toggle="dropdown">
                            <img src="{{asset('dashboard_public/img/user2-160x160.jpg')}}"  class="img-circle elevation-2" alt="User Image">
                            <span class="hidden-xs">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            {{--<!-- User image -->--}}
                            <li class="user-header">
                                <img src="{{asset('dashboard_public/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                            
                                <p style="    margin-left: 20%;">
                                    {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                                </p>
                            </li>
                            {{--<!-- Menu Footer-->--}}
                            <li class="user-footer">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat dropdown-item" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">@lang('site.logout')</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </li>
                        </ul>
                    </li>
    </ul>

    @else

    <ul class="navbar-nav ml-auto">
    {{--<!-- Tasks: style can be found in dropdown.less -->--}}
                       <li class="nav-item dropdown">
                        <a href="#" class="nav-link" data-toggle="dropdown"><i class="fa fa-flag"></i></a>
                        <ul class="dropdown-menu">
                            <li>
                                {{--<!-- inner menu: contains the actual data -->--}}
                                <ul class="nav  p-1">
                                    @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                        <li>
                                            <a class="dropdown-item"rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                                {{ $properties['native'] }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
    
      {{--<!-- User Account: style can be found in dropdown.less -->--}}
                    <li class="nav-item dropdown"  style="margin-top: -2%;">
                        <a href="#" class="nav-link user-panel" data-toggle="dropdown">
                            <img src="{{asset('dashboard_public/img/user2-160x160.jpg')}}"  class="img-circle elevation-2" alt="User Image">
                            <span class="hidden-xs">{{ auth()->user()->first_name }} {{ auth()->user()->last_name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            {{--<!-- User image -->--}}
                            <li class="user-header">
                                <img src="{{asset('dashboard_public/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image">
                            
                                <p style="    margin-left: 20%;">
                                    {{ auth()->user()->first_name }} {{ auth()->user()->last_name }}
                                </p>
                            </li>
                            {{--<!-- Menu Footer-->--}}
                            <li class="user-footer">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat dropdown-item" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">@lang('site.logout')</a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>

                            </li>
                        </ul>
                    </li>
    </ul>
    @endif

  </nav>
  <!-- /.navbar -->
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('dashboard_public/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{route('dashboard.welcome')}}" class="d-block">{{auth()->user()->first_name . ' ' . auth()->user()->last_name}}</a>
        </div>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->


         <!-- Dashboard الرثيسيه -->
          <li class="nav-item">
            <a href="{{route('dashboard.welcome')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right' class="nav-icon fas fa-tachometer-alt"></i>
              @else
              <i class="nav-icon fas fa-tachometer-alt"></i>
              @endif
              <p>
              @lang('site.dashboard')
              </p>
            </a>
            </li>



                 
        @if(auth()->user()->haspermission('categories_read'))
            <!-- الأقسام categoris -->
            <li class="nav-item">
              <a href="{{route('dashboard.categories.index')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right' class="nav-icon ion ion-bag"></i>
              @else
              <i class="nav-icon ion ion-bag"></i>
              @endif
              <p>
              @lang('site.categories')
              </p>
            </a>
          </li>
        @endif


        @if(auth()->user()->haspermission('products_read'))
            <!-- المنتجات products -->
            <li class="nav-item">
              <a href="{{route('dashboard.products.index')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right'  class="nav-icon ion ion-stats-bars "></i>
              @else
              <i class="nav-icon ion ion-stats-bars "></i>
              @endif
              <p>
              @lang('site.products')
              </p>
            </a>
          </li>
        @endif




        

        @if(auth()->user()->haspermission('clients_read'))
            <!-- العملاء clients -->
            <li class="nav-item">
              <a href="{{route('dashboard.clients.index')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right'  class="nav-icon fa fa-user"></i>
              @else
              <i class="nav-icon fa fa-user"></i>
              @endif
              <p>
              @lang('site.clients')
              </p>
            </a>
          </li>
        @endif


        
        @if(auth()->user()->haspermission('orders_read'))
            <!-- الطلبات orders -->
            <li class="nav-item">
              <a href="{{route('dashboard.orders.index')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right'  class="nav-icon fa fa-bookmark"></i>
              @else
              <i class="nav-icon fa fa-bookmark"></i>
              @endif
              <p>
              @lang('site.orders')
              </p>
            </a>
          </li>
        @endif
        


       

      
    

        @if(auth()->user()->haspermission('users_read'))
            <!-- المشرفين users -->
            <li class="nav-item">
            <a href="{{route('dashboard.users.index')}}" class="nav-link">
            @if (app() -> getlocale() == 'ar')
              <i style='float: right' class="nav-icon fas fa-users"></i>
              @else
              <i class="nav-icon fas fa-users"></i>
              @endif
              <p>
              @lang('site.users')
              </p>
            </a>
          </li>
        @endif



         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  
 

 
 
