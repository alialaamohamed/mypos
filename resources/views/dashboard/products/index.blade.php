@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.products')</h1>
          </div><!-- /.col -->

          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 38%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item active"></i>@lang('site.products')</li>
            </ol>
            </div>
            @else
            <div style="margin-left: 38%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item active">@lang('site.products')</li>
            </ol>
            </div>
            @endif
          </div><!-- /.col -->
          
          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.products') <small>{{$products->total()}}</small> </h2>
              </div>
              <!-- /.card-header -->

              <!-- form start -->
              <form role="form">
                <div class="card-body">
                <!-- SEARCH FORM -->
    <form  action="{{route('dashboard.products.index')}}" method="get" >
    <div class="row">
      <div class="col-md-4">
        <input class="form-control" name="search" type="text" placeholder="@lang('site.Search')" aria-label="Search" value="{{request()->search}}">
        </div>


        <div class="col-md-4">
         <select name="category_id" class="form-control">
             <option value="">@lang('site.all_categories')</option>
            @foreach ($categories as $category)
            <option value="{{ $category->id }}" {{ request()->category_id == $category->id ? 'selected' : '' }}>

            @if (app() -> getlocale() == 'ar')
              {{ $category->name_ar }}
              @else
              {{ $category->name_en }}
            @endif
              </option>

            @endforeach
        </select>
    </div>


        <div class= "col-md-4">
          <button class="btn btn-primary" type="submit">@lang('site.Search')<i class="fas fa-search"></i></button>

      @if(auth()->user()->haspermission('users_create'))

         <a href="{{route('dashboard.products.create')}}" class='btn btn-primary'>@lang('site.add')<i class="fas fa-plus"></i></a>
     @else

        <a href="#" class='btn btn-info' disabled>@lang('site.add')<i class="fas fa-plus"></i></a>

     @endif

  
        </div>
      </div>
    </form><br>

            @if($products->count() > 0 )
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>#</th>
                  <th>@lang('site.prodact_name')</th>
                  <th>@lang('site.desc_name')</th>
                  <th>@lang('site.category_name')</th>
                  <th>@lang('site.image')</th>
                  <th>@lang('site.purchase_price')</th>
                  <th>@lang('site.sale_price')</th>
                  <th>@lang('site.profit_percent')%</th>
                  <th>@lang('site.stock')</th>
                  <th>@lang('site.action')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $index => $product)
                <tr>
                  <td>{{ $index +1 }}</td>
            
                  @if (app() -> getlocale() == 'ar')
                  <td>{{ $product -> titel_ar }}</td>
                  <td>{!! $product -> descraption_ar !!}</td>
                    @else
                  <td>{{ $product -> titel_en }}</td>
                  <td>{!! $product -> descraption_en !!}</td>
                  @endif

                  @if (app() -> getlocale() == 'ar') 
                  <td>{{ $product->category->name_ar}}</td>
                  @else
                  <td>{{ $product->category->name_en}}</td>
                  @endif

                  <td><img src="{{ $product->image_path }}" style="width: 100px;" class="img-thumbnail" alt=""></td>
                  <td>{{ $product -> purchase_price }}</td>
                  <td>{{ $product -> sale_price }}</td>
                  <td>{{ $product -> profit_percent }} % </td>
                  <td>{{ $product -> stock }}</td>
                  <td>

                @if(auth()->user()->haspermission('products_update'))
             
                  <a class="btn btn-primary btn-sm" href="{{route('dashboard.products.edit', $product->id) }}"><i class="fa fa-edit" ></i>@lang('site.edit')</a>
                @else

                  <a class="btn btn-info btn-sm" href="#" disabled><i class="fa fa-edit" ></i>@lang('site.edit')</a>

                @endif

                @if(auth()->user()->haspermission('products_delete'))
                  <form action="{{ route('dashboard.products.destroy' , $product->id) }}"  method="post">

                  {{ csrf_field() }}
                  {{ method_field('delete')}}

                  <button type="submit"  class="btn btn-danger delete btn-sm"><i class="fa fa-trash" ></i>@lang('site.delete')</button>

                  </form>
                  @else
                      <button   class="btn btn-danger btn-sm" disabled> <i class="fa fa-trash" ></i>@lang('site.delete')</button>
                  @endif
                  </td>
                  
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                  <th>#</th>
                  <th>@lang('site.prodact_name')</th>
                  <th>@lang('site.desc_name')</th>
                  <th>@lang('site.category_name')</th>
                  <th>@lang('site.image')</th>
                  <th>@lang('site.purchase_price')</th>
                  <th>@lang('site.sale_price')</th>
                  <th>@lang('site.profit_percent')%</th>
                  <th>@lang('site.stock')</th>
                  <th>@lang('site.action')</th>
                </tr>
                </tfoot>
              </table>


              {{$products->appends(request()->query())->links()}}

            @else
            <h2>@lang('site.no_deta_found')</h2>
            @endif

            
                </div>
                <!-- /.card-body -->

               
              </form>
            </div>
    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>
  
@endsection