
@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.products')</h1>
          </div><!-- /.col -->

         
          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 33%;">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.products.index')}}">@lang('site.products')</a></li>
                 <li class="breadcrumb-item active">@lang('site.edit')</li>

            </ol>
            </div>
            @else
            <div style="margin-left: 33%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.products.index')}}">@lang('site.products')</a></li>
                 <li class="breadcrumb-item active">@lang('site.edit')</li>

            </ol>
            </div>
            @endif
          </div><!-- /.col -->

          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.edit')</h2>
              </div>

              @include('partials._errors')

              <form role="form" action="{{route('dashboard.products.update', $product->id)}}"  method="post" enctype="multipart/form-data">

                {{csrf_field()}}

                {{method_field('put')}}


                <div class="card-body">
                    <div class="form-group">
                            <label>@lang('site.categories')</label>
                            <select name="category_id" class="form-control">
                                <option value="">@lang('site.all_categories')</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}" {{ $product->category_id== $category->id ? 'selected' : '' }}>{{ $category->name_ar}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label>@lang('site.titel_ar')</label>
                            <input class="form-control" name="titel_ar" type="text" value="{{$product->titel_ar}}">
                        </div>
 

                        <div class="form-group">
                                <label>@lang('site.descraption_ar')</label>
                                <textarea name="descraption_ar" class="form-control ckeditor">{{$product->descraption_ar}}</textarea>
                            </div>


                        <div class="form-group">
                            <label>@lang('site.titel_en')</label>
                            <input class="form-control" name="titel_en" type="text" value="{{$product->titel_en}}">
                        </div>

                      <div class="form-group">
                                <label>@lang('site.descraption_en')</label>
                                <textarea name="descraption_en" class="form-control ckeditor">{{$product->descraption_en}}</textarea>
                            </div>

                        <div class="form-group">
                            <label>@lang('site.image')</label>
                            <input class="form-control image" name="image" type="file">
                        </div>

                        <div class="form-group">
                            <img src="{{ $product->image_path}}""  style="width: 100px" class="img-thumbnail image-preview" alt="">
                        </div>



                        <div class="form-group">
                            <label>@lang('site.purchase_price')</label>
                            <input type="number" name="purchase_price" step="0.01" class="form-control" value="{{$product->purchase_price}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.sale_price')</label>
                            <input type="number" name="sale_price" step="0.01" class="form-control" value="{{$product->sale_price}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.stock')</label>
                            <input type="number" name="stock" class="form-control" value="{{$product->stock}}">
                        </div>



              
                         <div class="form-group">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                        </div>

              </div><!-- /.card-header -->  
            </div>


                         </div>
                   </div>
               </form>



            

    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   
  </div>


@endsection