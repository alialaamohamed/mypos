
@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.dashboard')</h1>
          </div><!-- /.col -->

          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 44%;">
          <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item active">@lang('site.dashboard')</li>
            </ol>
            </div>
            @else
            <div style="margin-left: 44%;">
          <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item active">@lang('site.dashboard')</li>
            </ol>
            </div>
            @endif
    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

              
          <section class="content">

              <div class="row">

                  {{-- categories--}}
                  <div class="col-lg-3 col-xs-6">
                      <div class="small-box bg-info">
                          <div class="inner">
                              <h3>{{ $categories_count }}</h3>

                              <p>@lang('site.categories')</p>
                          </div>
                          <div class="icon">
                              <i class="ion ion-bag"></i>
                          </div>
                          <a href="{{ route('dashboard.categories.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>

                  {{--products--}}
                  <div class="col-lg-3 col-xs-6">
                      <div class="small-box bg-success">
                          <div class="inner">
                              <h3>{{ $products_count }}</h3>

                              <p>@lang('site.products')</p>
                          </div>
                          <div class="icon">
                              <i class="ion ion-stats-bars"></i>
                          </div>
                          <a href="{{ route('dashboard.products.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>

                  {{--clients--}}
                  <div class="col-lg-3 col-xs-6">
                      <div class="small-box bg-warning">
                          <div class="inner">
                              <h3>{{ $clients_count }}</h3>

                              <p>@lang('site.clients')</p>
                          </div>
                          <div class="icon">
                              <i class="fa fa-user"></i>
                          </div>
                          <a href="{{ route('dashboard.clients.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>

                  {{--users--}}
                  <div class="col-lg-3 col-xs-6">
                      <div class="small-box bg-danger">
                          <div class="inner">
                              <h3>{{ $users_count }}</h3>

                              <p>@lang('site.users')</p>
                          </div>
                          <div class="icon">
                              <i class="fa fa-users"></i>
                          </div>
                          <a href="{{ route('dashboard.users.index') }}" class="small-box-footer">@lang('site.read') <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                  </div>

              </div><!-- end of row -->

                  <div class="card card-solid">

                      <div class="card-header">
                          <h3 class="card-title">Sales Graph</h3>
                      </div>
                      <div class="box-body border-radius-none">
                          <div class="chart" id="line-chart" style="height: 250px;"></div>
                      </div>
                      <!-- /.box-body -->
                  </div>

      </section><!-- end of content -->



    <!-- /.content -->
  </div>
@endsection

@push('scripts')

    <script>

        //line chart
        var line = new Morris.Line({
            element: 'line-chart',
            resize: true,
            data: [
                @foreach ($sales_data as $data)
                {
                    ym: "{{ $data->year }}-{{ $data->month }}", sum: "{{ $data->sum }}"
                },
                @endforeach
            ],
            xkey: 'ym',
            ykeys: ['sum'],
            labels: ['@lang('site.total')'],
            lineWidth: 2,
            hideHover: 'auto',
            gridStrokeWidth: 0.4,
            pointSize: 4,
            gridTextFamily: 'Open Sans',
            gridTextSize: 10
        });
    </script>

@endpush