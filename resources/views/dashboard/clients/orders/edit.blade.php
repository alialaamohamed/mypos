@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.edit_order')</h1>
          </div><!-- /.col -->

          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 31%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{ route('dashboard.clients.index') }}"> @lang('site.clients')</a></li>
                 <li class="breadcrumb-item active ">@lang('site.edit_order')</li>
            </ol>
            </div>
            @else
            <div style="margin-left: 86%;">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{ route('dashboard.clients.index') }}"> @lang('site.clients')</a></li>
                 <li class="breadcrumb-item active ">@lang('site.edit_order')</li>
            </ol>
            </div>
            @endif
          </div><!-- /.col -->
          
    
              <section class="content">

                  <div class="row">

                      <div class="col-md-6">

                          <div class="card card-primary card-outline">

                              <div class="card-header">

                                  <h3 class="card-title" style="margin-bottom: 10px">@lang('site.categories')</h3>

                              </div><!-- end of box header -->

                              <div class="box-body">

                                  @foreach ($categories as $category)

                                      <div class="form-group">
                                          <div class="card card-light">
                                              <div class="card-header">
                                                  <h4 class="card-title">
                                                  @if (app() -> getlocale() == 'ar')
                                                      <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name_ar) }}">{{ $category->name_ar }}</a>
                                                  @else
                                                  <a data-toggle="collapse" href="#{{ str_replace(' ', '-', $category->name_en) }}">{{ $category->name_en }}</a>

                                                  @endif
                                                  </h4>
                                              </div>

                                                                       
                                        @if (app() -> getlocale() == 'ar')
                                        <div id="{{ str_replace(' ', '-', $category->name_ar) }}" class="panel-collapse collapse">
                                        @else
                                        <div id="{{ str_replace(' ', '-', $category->name_en) }}" class="panel-collapse collapse">

                                        @endif
                                                  <div class="card-body">

                                                      @if ($category->products->count() > 0)

                                                          <table class="table table-hover">
                                                              <tr>
                                                                  <th>@lang('site.name')</th>
                                                                  <th>@lang('site.stock')</th>
                                                                  <th>@lang('site.price')</th>
                                                                  <th>@lang('site.add')</th>
                                                              </tr>

                                                              @foreach ($category->products as $product)
                                                                  <tr>
                                                                    @if (app() -> getlocale() == 'ar')
                                                                     <td>{{ $product->titel_ar }}</td>
                                                                     @else
                                                                     <td>{{ $product->titel_en }}</td>
                                                                     @endif
                                                                      <td>{{ $product->stock }}</td>
                                                                      <td>{{ $product->sale_price }}</td>

                                                                      @if (app() -> getlocale() == 'ar')
                                                                      <td>
                                                                          <a href=""
                                                                            id="product-{{ $product->id }}"
                                                                            data-name="{{ $product->titel_ar }}"
                                                                            data-id="{{ $product->id }}"
                                                                            data-price="{{ $product->sale_price }}"
                                                                            class="btn {{ in_array($product->id, $order->products->pluck('id')->toArray()) ? 'btn-default disabled' : 'btn-success add-product-btn' }} btn-sm">
                                                                              <i class="fa fa-plus"></i>
                                                                          </a>
                                                                      </td>
                                                                      @else
                                                                      <td>
                                                                          <a href=""
                                                                            id="product-{{ $product->id }}"
                                                                            data-name="{{ $product->titel_en }}"
                                                                            data-id="{{ $product->id }}"
                                                                            data-price="{{ $product->sale_price }}"
                                                                            class="btn {{ in_array($product->id, $order->products->pluck('id')->toArray()) ? 'btn-default disabled' : 'btn-success add-product-btn' }} btn-sm">
                                                                              <i class="fa fa-plus"></i>
                                                                          </a>
                                                                      </td>

                                                                      @endif


                                                                  </tr>
                                                              @endforeach

                                                          </table><!-- end of table -->

                                                      @else
                                                          <h5>@lang('site.no_records')</h5>
                                                      @endif

                                                  </div><!-- end of panel body -->

                                              </div><!-- end of panel collapse -->

                                          </div><!-- end of panel primary -->

                                      </div><!-- end of panel group -->

                                  @endforeach

                              </div><!-- end of box body -->

                          </div><!-- end of box -->

                      </div><!-- end of col -->

                      <div class="col-md-6">

                          <div class="card card-primary card-outline">

                              <div class="card-header">

                                  <h3 class="card-title">@lang('site.orders')</h3>

                              </div><!-- end of box header -->

                              <div class="card-body">

                                  @include('partials._errors')

                                  <form action="{{ route('dashboard.clients.orders.update', ['order' => $order->id, 'client' => $client->id]) }}" method="post">

                                      {{ csrf_field() }}
                                      {{ method_field('put') }}

                                      <table class="table table-hover">
                                          <thead>
                                          <tr>
                                              <th>@lang('site.product')</th>
                                              <th>@lang('site.quantity')</th>
                                              <th>@lang('site.price')</th>
                                          </tr>
                                          </thead>

                                          <tbody class="order-list">

                                          @foreach ($order->products as $product)
                                              <tr>
                                              @if (app() -> getlocale() == 'ar')
                                                  <td>{{ $product->titel_ar }}</td>
                                                @else
                                                <td>{{ $product->titel_en }}</td>
                                                @endif
                                                  
                                                  <td><input type="number" name="products[{{ $product->id }}][quantity]" data-price="{{ number_format($product->sale_price, 2) }}" class="form-control input-sm product-quantity" min="1" value="{{ $product->pivot->quantity }}"></td>
                                                  <td class="product-price">{{ number_format($product->sale_price * $product->pivot->quantity, 2) }}</td>
                                                  <td>
                                                      <button class="btn btn-danger btn-sm remove-product-btn" data-id="{{ $product->id }}"><span class="fa fa-trash"></span></button>
                                                  </td>
                                              </tr>
                                          @endforeach

                                          </tbody>

                                      </table><!-- end of table -->

                                      <h4>@lang('site.total') : <span class="total-price">{{ number_format($order->total_price, 2) }}</span></h4>

                                      <button class="btn btn-primary btn-block" id="form-btn"><i class="fa fa-edit"></i> @lang('site.edit_order')</button>

                                  </form><!-- end of form -->

                              </div><!-- end of box body -->

                          </div><!-- end of box -->

                          @if ($client->orders->count() > 0)

                              <div class="card card-primary card-outline">

                                  <div class="card-header">

                                      <h3 class="card-title" style="margin-bottom: 10px">@lang('site.previous_orders')
                                          <small>{{ $orders->total() }}</small>
                                      </h3>

                                  </div><!-- end of box header -->

                                  <div class="box-body">

                                      @foreach ($orders as $order)

                                          <div class="panel-group">

                                              <div class="card card-success">

                                                  <div class="card-header">
                                                      <h4 class="card-title">
                                                          <a data-toggle="collapse" href="#{{ $order->created_at->format('d-m-Y-s') }}">{{ $order->created_at->toFormattedDateString() }}</a>
                                                      </h4>
                                                  </div>

                                                  <div id="{{ $order->created_at->format('d-m-Y-s') }}" class="panel-collapse collapse">

                                                      <div class="panel-body">

                                                          <ul class="list-group">
                                                              @foreach ($order->products as $product)
                                                                  <li class="list-group-item">{{ $product->titel_ar }}</li>
                                                              @endforeach
                                                          </ul>

                                                      </div><!-- end of panel body -->

                                                  </div><!-- end of panel collapse -->

                                              </div><!-- end of panel primary -->

                                          </div><!-- end of panel group -->

                                      @endforeach

                                      {{ $orders->links() }}

                                  </div><!-- end of box body -->

                              </div><!-- end of box -->

                          @endif

                      </div><!-- end of col -->

                  </div><!-- end of row -->

            </section><!-- end of content -->



        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>
  
@endsection