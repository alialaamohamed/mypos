
@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.categories')</h1>
          </div><!-- /.col -->

         
          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 33%;">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.categories.index')}}">@lang('site.categories')</a></li>
                 <li class="breadcrumb-item active"><i class="nav-icon fas fa-plus"></i>@lang('site.add')</li>

            </ol>
            </div>
            @else
            <div style="margin-left: 33%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.categories.index')}}">@lang('site.categories')</a></li>
                 <li class="breadcrumb-item active">@lang('site.add')</li>

            </ol>
            </div>
            @endif
          </div><!-- /.col -->

          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.add')</h2>
              </div>

              @include('partials._errors')

              <form role="form" action="{{route('dashboard.categories.store')}}"  method="post">

                {{csrf_field()}}

                {{method_field('post')}}

                   <div class="card-body">

                   <div class="card-body">
                        <div class="form-group">
                            <label>@lang('site.name_ar')</label>
                            <input class="form-control" name="name_ar" type="text" value="{{old('name_ar')}}">
                        </div>

                        
                        <div class="form-group">
                            <label>@lang('site.name_en')</label>
                            <input class="form-control" name="name_en" type="text" value="{{old('name_en')}}">
                        </div>


          


                        <div class="form-group">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('site.add')</button>
                        </div>

            
                </div>
           
              </div><!-- /.card-body -->
            </div>
                         </div>


                       
                   </div>
               </form>



            

    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   
  </div>


@endsection