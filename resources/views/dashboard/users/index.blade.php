@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.users')</h1>
          </div><!-- /.col -->

          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 38%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item active"></i>@lang('site.users')</li>
            </ol>
            </div>
            @else
            <div style="margin-left: 38%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item active">@lang('site.users')</li>
            </ol>
            </div>
            @endif
          </div><!-- /.col -->
          
          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.users') <small>{{$users->total()}}</small> </h2>
              </div>
              <!-- /.card-header -->

              <!-- form start -->
              <form role="form">
                <div class="card-body">
                <!-- SEARCH FORM -->
     <form action="{{route('dashboard.users.index')}}" method="get" >
     <div class="row">

      <div class="col-md-4">
        <input class="form-control" name="search" type="text" placeholder="@lang('site.Search')" aria-label="Search" value="{{request()->search}}">
        </div>

        <div class="col-md-4">
          <button class="btn btn-primary" type="submit">@lang('site.Search')<i class="fas fa-search"></i></button>

       @if(auth()->user()->haspermission('users_create'))
         <a href="{{route('dashboard.users.create')}}" class='btn btn-primary'>@lang('site.add')<i class="fas fa-plus"></i></a>
       @else
          <a href="#" class='btn btn-info' disabled>@lang('site.add')<i class="fas fa-plus"></i></a>
      @endif
        </div>
      </div>
      </div>
    </form><br>

            @if($users->count() > 0 )
            <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                <th>#</th>
                  <th>@lang('site.first_name')</th>
                  <th>@lang('site.last_name')</th>
                  <th>@lang('site.email')</th>
                  <th>@lang('site.image')</th>
                  <th>@lang('site.action')</th>
                </tr>
                </thead>
                <tbody>
                @foreach($users as $index => $user)
                <tr>
                  <td>{{ $index +1 }}</td>
                  <td>{{ $user -> first_name }}</td>
                  <td>{{ $user -> last_name }}</td>
                  <td>{{ $user -> email }}</td>
                  <td><img src="{{ $user-> image_path }}" style="width: 100px;" class="img-thumbnail" alt=""></td>
                  <td>

                  @if(auth()->user()->haspermission('users_update'))

                  @if (app() -> getlocale() == 'ar')
                  <a class="btn btn-primary btn-sm" href="{{route('dashboard.users.edit', $user->id) }}" style="float: right;"><i class="fa fa-edit" ></i>@lang('site.edit')</a>
                    @else
                    <a class="btn btn-primary btn-sm" href="{{route('dashboard.users.edit', $user->id) }}" style="float: left;"><i class="fa fa-edit" ></i>@lang('site.edit')</a>
                  @endif



                  @else
                  <a class="btn btn-info btn-sm" href="#" disabled><i class="fa fa-edit" ></i>@lang('site.edit')</a>
                @endif

                @if(auth()->user()->haspermission('users_delete'))
                  <form action="{{ route('dashboard.users.destroy' , $user->id) }}"  method="post">

                  {{ csrf_field() }}
                  {{ method_field('delete')}}


                  @if (app() -> getlocale() == 'ar')
                  <button type="submit"  class="btn btn-danger delete btn-sm" style="margin-right:3%;"><i class="fa fa-trash" ></i>@lang('site.delete')</button>
                  @else
                  <button type="submit"  class="btn btn-danger delete btn-sm" style="margin-left:3%;"><i class="fa fa-trash" ></i>@lang('site.delete')</button>
                    @endif   


                  </form>

                  @else
                      <button   class="btn btn-danger btn-sm" disabled> <i class="fa fa-trash" ></i>@lang('site.delete')</button>
                  
                  @endif

                  </td>
                </tr>
                @endforeach
                </tbody>
                <tfoot>
                <tr>
                <th>#</th>
                  <th>@lang('site.first_name')</th>
                  <th>@lang('site.last_name')</th>
                  <th>@lang('site.email')</th>
                  <th>@lang('site.image')</th>
                  <th>@lang('site.action')</th>
                </tr>
                </tfoot>
              </table>


              {{$users->appends(request()->query())->links()}}

            @else
            <h2>@lang('site.no_deta_found')</h2>
            @endif

            
                </div>
                <!-- /.card-body -->

               
              </form>
            </div>
    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
  </div>
  
@endsection