
@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.users')</h1>
          </div><!-- /.col -->

         
          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 33%;">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.users.index')}}">@lang('site.users')</a></li>
                 <li class="breadcrumb-item active">@lang('site.edit')</li>

            </ol>
            </div>
            @else
            <div style="margin-left: 33%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.users.index')}}">@lang('site.users')</a></li>
                 <li class="breadcrumb-item active">@lang('site.edit')</li>

            </ol>
            </div>
            @endif
          </div><!-- /.col -->

          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.edit')</h2>
              </div>

              @include('partials._errors')

              <form role="form" action="{{route('dashboard.users.update',$user->id)}}"  method="post"  enctype="multipart/form-data">

                {{csrf_field()}}

                {{method_field('put')}}



                   <div class="card-body">
                        <div class="form-group">
                            <label>@lang('site.first_name')</label>
                            <input class="form-control" name="first_name" type="text" value="{{$user->first_name}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.last_name')</label>
                            <input class="form-control" name="last_name" type="text" value="{{$user->last_name}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.email')</label>
                            <input class="form-control" name="email" type="email" value="{{$user->email}}">
                        </div>


                        <div class="form-group">
                            <label>@lang('site.image')</label>
                            <input class="form-control image" name="image" type="file">
                        </div>

                        <div class="form-group">
                            <img src="{{ $user->image_path}}"  style="width: 100px" class="img-thumbnail image-preview" alt="">
                        </div>




                        <div class="form-group">
                        <label>@lang('site.permissions')</label>

                      @php

                           $models = ['users','categories','products','clients','orders'];
                           $crud   = ['create','read','update','delete']

                      @endphp

                         <div class="card">
              <div class="card d-flex p-0">

              @if (app() -> getlocale() == 'ar')
                <ul class="nav nav-pills ml-auto p-2 active">
                @foreach($models as $index => $model)
                <li class="nav-item"><a class="nav-link {{$index == 0 ? 'active' : '' }}" href="#{{$model}}" data-toggle="tab">@lang('site.' .$model )</a></li>
                @endforeach

                @else
                <ul class="nav nav-pills ml-auto p-2 active" style="margin-right: 77%;">
                @foreach($models as $index => $model)
                <li class="nav-item"><a class="nav-link {{$index == 0 ? 'active' : '' }}" href="#{{$model}}" data-toggle="tab">@lang('site.' .$model )</a></li>
                @endforeach

                @endif


                </ul>
              </div><!-- /.card-header -->  
              <div class="card-body">
                <div class="tab-content"  >

                  @foreach($models as $index => $model)

                  <div class="tab-pane {{$index == 0 ? 'active' : '' }}" id="{{$model}}">

                  @foreach($crud as $prem)

                  <input name="permissions[]" type="checkbox" {{$user->haspermission($model. '_' .$prem ) ? 'checked' : ''}}  value="{{$model. '_' .$prem  }}"> <label> @lang('site.' .$prem) </label>


                  @endforeach
                     
                  </div>

                  @endforeach
            
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>


                         </div>


                        <div class="form-group">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-edit"></i>@lang('site.edit')</button>
                        </div>
                   </div>
               </form>



            

    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   
  </div>


@endsection