
@extends('layouts.dashboard.app')

@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">@lang('site.users')</h1>
          </div><!-- /.col -->

         
          @if (app() -> getlocale() == 'ar')
          <div style="margin-right: 33%;">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.users.index')}}">@lang('site.users')</a></li>
                 <li class="breadcrumb-item active">@lang('site.add')</li>

            </ol>
            </div>
            @else
            <div style="margin-left: 33%;">
            <ol class="breadcrumb float-sm-right">
                 <li class="breadcrumb-item"><a href="{{route('dashboard.welcome')}}">@lang('site.dashboard')</a></li>
                 <li class="breadcrumb-item "><a href="{{route('dashboard.users.index')}}">@lang('site.users')</a></li>
                 <li class="breadcrumb-item active">@lang('site.add')</li>

            </ol>
            </div>
            @endif
          </div><!-- /.col -->

          <div class="card card-info card-outline">
              <div class="card-header">
                <h2 class="card-title">@lang('site.add')</h2>
              </div>

              @include('partials._errors')

              <form role="form" action="{{route('dashboard.users.store')}}"  method="post" enctype="multipart/form-data">

                {{csrf_field()}}

                {{method_field('post')}}



                   <div class="card-body">
                        <div class="form-group">
                            <label>@lang('site.first_name')</label>
                            <input class="form-control" name="first_name" type="text" value="{{old('first_name')}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.last_name')</label>
                            <input class="form-control" name="last_name" type="text" value="{{old('last_name')}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.email')</label>
                            <input class="form-control" name="email" type="email" value="{{old('email')}}">
                        </div>

                        
                        <div class="form-group">
                            <label>@lang('site.image')</label>
                            <input class="form-control image" name="image" type="file">
                        </div>

                        <div class="form-group">
                            <img src="{{ asset('uploads/user_image/default.png') }}"  style="width: 100px" class="img-thumbnail image-preview" alt="">
                        </div>



                        <div class="form-group">
                            <label>@lang('site.password')</label>
                            <input class="form-control" name="password" type="password" value="{{old('password')}}">
                        </div>

                        <div class="form-group">
                            <label>@lang('site.password_confirmation')</label>
                            <input class="form-control" name="password_confirmation" type="password" value="{{old('password_confirmation')}}">
                        </div>


                        <div class="form-group">
                        <label>@lang('site.permissions')</label>

                      @php

                           $models = ['users','categories','products','clients','orders'];
                           $crud   = ['create','read','update','delete']

                      @endphp

                         <div class="card">
              <div class="card d-flex p-0">

              @if (app() -> getlocale() == 'ar')
                <ul class="nav nav-pills ml-auto p-2 active">
                @foreach($models as $index => $model)
                <li class="nav-item"><a class="nav-link {{$index == 0 ? 'active' : '' }}" href="#{{$model}}" data-toggle="tab">@lang('site.' .$model )</a></li>
                @endforeach
                </ul>

                @else
                <ul class="nav nav-pills ml-auto p-2 active" style="margin-right: 77%;">
                @foreach($models as $index => $model)
                <li class="nav-item"><a class="nav-link {{$index == 0 ? 'active' : '' }}" href="#{{$model}}" data-toggle="tab">@lang('site.' .$model )</a></li>
                @endforeach
                </ul>
                @endif

              </div><!-- /.card-header -->  
              <div class="card-body">
                <div class="tab-content"  >

                  @foreach($models as $index => $model)

                  <div class="tab-pane {{$index == 0 ? 'active' : '' }}" id="{{$model}}">

                  @foreach($crud as $prem)

                  <input name="permissions[]" type="checkbox"  value="{{$model. '_' .$prem  }}"> <label> @lang('site.' .$prem) </label>


                  @endforeach
                     
                  </div>

                  @endforeach
            
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>


                         </div>


                        <div class="form-group">
                          <button type="submit" class="btn btn-primary"><i class="fa fa-plus"></i>@lang('site.add')</button>
                        </div>
                   </div>
               </form>



            

    
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
   
  </div>


@endsection