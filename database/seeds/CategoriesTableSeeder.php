<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $categories = ['cat one', 'cat two', 'cat three'];

        foreach ($categories as $category) {

            \App\Category::create([
                'name_ar' => $category . 'ar',
                'name_en' => $category . 'en',
              
            ]);

        }//end of foreach

    }//end of run
}//end of seeder