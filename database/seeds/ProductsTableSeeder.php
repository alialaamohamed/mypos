<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = ['pro one', 'pro two' ,'pro three'];

        foreach ($products as $product) {

            \App\Product::create([
                'category_id'    => 1,
                'titel_ar'       =>$product . 'ar',
                'descraption_ar' =>$product . 'desc ar',
                'titel_en'       =>$product  . 'en',
                'descraption_en' =>$product  . 'desc en',
                'purchase_price' => 100,
                'sale_price'     => 150,
                'stock'          => 100,
            ]);

        }//end of foreach

    }//end of run
}
